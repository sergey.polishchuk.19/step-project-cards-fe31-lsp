import API from "../api/api.js";
import Form from "../form/form.js";
import * as Function from '../constans/index.js'


export class Card {
    constructor(user, cardColor, doctorObj) {
        this._ELEMENTS = {
            moreBtn: document.createElement('button'),
            delBtn: document.createElement('button')
        }
        this.FIO = user.fullName
        this.desc = user.description
        this.target = user.target
        this.urgency = user.urgency
        this.doctor = user.doctor
        this.cardColor = cardColor
        this.diseases = user.diseases
        this.massIndex = user.bodyMassIndex
        this.age = user.age
        this.ID = user.id
        this.doctorObj = doctorObj
    }

    render(parent) {
        let {moreBtn, delBtn} = this._ELEMENTS
        parent.insertAdjacentHTML('afterbegin', `${Function.makeCardCarcass(this.ID, this.cardColor, this.FIO, this.doctor)}`)
        if (this.doctor === 'Cardiologist') {
            this.doctorObj.showCardiologist(document.body.querySelector('.first-half'))
        }
        if (this.doctor === 'Dentist') {
            this.doctorObj.showDentist(document.body.querySelector('.first-half'))
        }
        if (this.doctor === "Therapist") {
            this.doctorObj.showTherapist(document.body.querySelector('.first-half'))
        }
        delBtn.classList.add('btn-close')
        delBtn.classList.add('delete-card')
        moreBtn.classList.add('btn-more')
        moreBtn.classList.add('show-more')
        document.body.querySelector('.second-half').insertAdjacentElement('afterend', moreBtn)
        document.querySelector('.card').prepend(delBtn)
        moreBtn.addEventListener('click', () => {
            if (moreBtn.classList.contains('show-more')) {
                moreBtn.classList.toggle('show-more')
                document.getElementById(`${this.ID}`).querySelector('.second-half').style.display = 'block'
            } else {
                moreBtn.classList.toggle('show-more')
                document.getElementById(`${this.ID}`).querySelector('.second-half').style.display = 'none'
            }
        })
        delBtn.addEventListener('click', () => {
            if (confirm("Are u sure?")) {
                API.removeCards(this.ID)
                document.getElementById(`${this.ID}`).remove()
            }
        })
        document.querySelector('.edit-img').addEventListener('click', () => {
            if (document.querySelector('#card-modal .visit-wrapper')) {
                document.querySelector('#card-modal .visit-wrapper').remove()
                this.editCard()
            } else {
                this.editCard()
            }
        })
    }

    editCard() {
        const editForm = new Form()
        document.querySelector('.modal').style.display = 'block'
        document.querySelector('#card-modal').classList.add('show')
        editForm.visit(document.querySelector('.modal-content'), 'card-modal')
        editForm.editCard(this.ID)

    }

    madeColor(urgency) {
        const colorBg = {
            Low: '#FFCD00',
            Normal: '#FFB273',
            High: '#BF3030'
        }
        if (urgency === 'Low') {
            return colorBg.Low
        } else if (urgency === 'Normal') {
            return colorBg.Normal
        } else if(urgency === 'High'){
            return colorBg.High
        }
    }

}
