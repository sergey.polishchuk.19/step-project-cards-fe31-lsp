import Visit from "./visit.js";
import * as Function from '../constans/index.js'

export class VisitTherapist extends Visit {
    constructor(
        target,
        description,
        urgency,
        fullName,
        doctor,
        age,
        comments) {
        super(target, description, urgency, fullName, doctor, comments);
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.doctor = doctor
        this.age = age
    }

    showTherapist(parent) {
        parent.insertAdjacentHTML('afterend', `
        ${Function.makeTherapistBlock(this.urgency, this.target, this.description, this.age, this.comments )}
        `)
    }
}