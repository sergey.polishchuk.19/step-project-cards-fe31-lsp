import * as CONST from '../constans';

export default class Modal {
    constructor(id) {
        this.id = id
        this._ELEMENTS = {
            window: CONST.DEFAULT_MODAL(id)
        }
    }

    render() {
        const {window} = this._ELEMENTS
        const parent = document.querySelector('.modal-container')

        parent.insertAdjacentHTML('afterbegin', window)
    }

    appendBody(content) {
        const parent = document.querySelector(`#${this.id} .modal-body`);
        parent.append(content);
    }
}
