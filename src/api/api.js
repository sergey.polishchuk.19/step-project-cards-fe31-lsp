export default class API {
    static async auth(user) {
        return await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
            body: JSON.stringify(user)
        })
            .then(r => {
                if (r.status >= 400) {
                    throw new Error('wrong creds')
                } else return r
            })
            .then(response => response.text())
    }

    static async addCard(card) {
        const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
            body: JSON.stringify(card)
        }).then(res => res.json())
        return response
    }

    static async getAllCards() {
           let resp = await fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
        }).then(res => res.json())
        return await resp
    }

    static async removeCards(id) {
        return await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
        }).then(res => res.text())
    }

    static async editCard(newCard) {
        return await fetch(`https://ajax.test-danit.com/api/v2/cards/${newCard.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
            body: JSON.stringify(newCard)
        })
            .then(response => response.json())
    }
    static async getSingleCard(ID){
        return await fetch(`https://ajax.test-danit.com/api/v2/cards/${ID}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            }
        }).then(res => res.json())
    }
}
