export const DEFAULT_MODAL = (id) => (`
    <div class="modal fade" id=${id} data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content my-window">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
    `)


export function makeCardCarcass(id, color, fio, doctor){
    const carcass = `<div id = '${id}' class="card text-white mb-3" style= "background: ${color}">
            <div class="card-header">Card of patient</div>
            <svg class = 'edit-img' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
  <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
</svg>
            <div class="card-body">
            <div class="first-half">
                <h5 class="card-title">Patient: <br/> ${fio}</h5>
                <p class="card-text" id="doc">Doctor: ${doctor}</p>
            </div>
            </div>
        </div>`
    return carcass
}


export function makeCardiologistBlock(urgency, target, description, diseases, bodyMassIndex, normalPressure, age, comments){
    const cardioBlock = `<div class="second-half">
                <p class="card-text">Target-urgency: ${urgency}</p>
                <p class="card-text">Visit-target: ${target}</p>
                <p class="card-text">Description: ${description}</p>
                <p class="card-text">Diseases: ${diseases}</p>
                <p class="card-text">Body index mass: ${bodyMassIndex}</p>
                <p class="card-text">Pressure: ${normalPressure}</p>
                <p class="card-text">Age: ${age}</p>
                <p class="card-text">Comments: ${comments}</p>
         </div>`
    return cardioBlock
}

export function makeDentistBlock(urgency, target, description, lastVisit, comments){
    const dentistBlock = `<div class="second-half">
                <p class="card-text">Target-urgency: ${urgency}</p>
                <p class="card-text">Visit-target: ${target}</p>
                <p class="card-text">Description: ${description}</p>
                <p class="card-text">Last visit: ${lastVisit}</p>
                <p class="card-text">Comments: ${comments}</p>
        </div>`
    return dentistBlock
}

export function makeTherapistBlock(urgency, target, description, age, comments){
    const therapistBlock = `<div class="second-half">
                <p class="card-text">Target-urgency: ${urgency}</p>
                <p class="card-text">Visit-target: ${target}</p>
                <p class="card-text">Description: ${description}</p>
                <p class="card-text">Age: ${age}</p>
                <p class="card-text">Comments: ${comments}</p>
        </div>`
    return therapistBlock
}